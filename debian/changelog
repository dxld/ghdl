ghdl (2.0.0+dfsg-1) unstable; urgency=medium

  * Add excluded files for 2.0.0 release
  * New upstream version 2.0.0+dfsg
  * Fix build for upstream git
  * Fix unclean rebuild via debian/rules
  * Enable and install libghdl
  * Fix ghdl --libghdl-* paths
  * Fix packaging for 2.0.0

 -- Daniel Gröber <dxld@darkboxed.org>  Sun, 13 Mar 2022 11:26:27 +0100

ghdl (1.0.0+dfsg-8) unstable; urgency=medium

  * Add Depends on gcc to ghdl-gcc and ghdl-llvm packages as that is used by
    default for linking simulations
  * Add gcc and libc6-dev to autopkgtest Depends for ghdl-mcode as that is
    used by the testsuite for VPI tests

 -- Andreas Bombe <aeb@debian.org>  Tue, 15 Feb 2022 01:38:02 +0100

ghdl (1.0.0+dfsg-7) unstable; urgency=medium

  * Run testsuite during package build
  * Add autopkgtest configuration
  * Use current name of shared-lib-without-dependency-information lintian
    override
  * Update years in d/copyright

 -- Andreas Bombe <aeb@debian.org>  Sun, 13 Feb 2022 18:11:29 +0100

ghdl (1.0.0+dfsg-6) unstable; urgency=medium

  * Add llvm13.0.patch to allow building with LLVM 13.0 (Closes: #1000923)
  * Change versioning patch to the more general variant that was upstreamed
  * Refresh llvm11.1.0.patch
  * Add Forwarded tags to Debian patches

 -- Andreas Bombe <aeb@debian.org>  Wed, 22 Dec 2021 03:50:59 +0100

ghdl (1.0.0+dfsg-5) unstable; urgency=medium

  * Add llvm11.1.0.patch to enable building with LLVM 11.1 (Closes: #997729)

 -- Jonathan McDowell <noodles@earth.li>  Tue, 02 Nov 2021 08:49:46 +0000

ghdl (1.0.0+dfsg-4) unstable; urgency=medium

  * Add llvm12.0.patch from Ubuntu to enable building with LLVM 12
    (Closes: #989081)
  * Refresh patches
  * Bump Standards-Version to 4.6.0, no changes needed

 -- Andreas Bombe <aeb@debian.org>  Tue, 14 Sep 2021 01:51:47 +0200

ghdl (1.0.0+dfsg-3) unstable; urgency=medium

  * Fix Built-Using field just introduced in ghdl-gcc, it needs to record the
    source instead of the binary package name (Closes: 987528)

 -- Andreas Bombe <aeb@debian.org>  Sun, 25 Apr 2021 23:07:51 +0200

ghdl (1.0.0+dfsg-2) unstable; urgency=medium

  * Add Built-Using field to ghdl-gcc to record use of gcc source package
    (Closes: 987528)

 -- Andreas Bombe <aeb@debian.org>  Sun, 25 Apr 2021 20:16:13 +0200

ghdl (1.0.0+dfsg-1) unstable; urgency=medium

  * New upstream version 1.0.0+dfsg
  * Update d/copyright for 1.0.0 changes and exclude some more testsuites
  * Adapt versioning patch to changes in Makefile.in
  * Remove .../vhdl/synopsys and .../vhdl/mentor directories from
    ghdl-*.install, they are not installed separately anymore
  * Add .../vhdl/grt-*.* to d/ghdl-gcc.install and d/ghdl-llvm.install
  * Drop allow-llvm11 patch, already allowed with new upstream version
  * Drop patch remove-vital-from-build, use configure option
    --enable-gplcompat instead
  * Add clean-more patch so that "make distclean" really cleans up everything
  * Override dh_auto_clean and dh_auto_test to prevent debhelper errors about
    python-distutils, presumably because of pyGHDL which isn't enabled in the
    package yet
  * Exclude an .orig file that exists in the upstream release from dh_clean

 -- Andreas Bombe <aeb@debian.org>  Thu, 11 Feb 2021 02:29:19 +0100

ghdl (0.37+dfsg2-2) unstable; urgency=medium

  * Add Breaks+Replaces ghdl (<< 0.37+dfsg2) on ghdl-common (Closes: #981576)
  * Do not install LICENSE file into /usr/lib/ghdl/src/ieee2008

 -- Andreas Bombe <aeb@debian.org>  Wed, 03 Feb 2021 02:40:10 +0100

ghdl (0.37+dfsg2-1) unstable; urgency=medium

  * Repack as 0.37+dfsg2
    * Add exclusion of some testsuite files in DFSG repack
    * Stop excluding libraries/ieee and libraries/ieee2008 in DFSG repack as
      they are now under a DFSG-free license (Apache 2.0)
    * Use the now DFSG-free IEEE libraries instead of GHDL's OpenIEEE
      substitute (Closes: #929656)
    * Add patch remove-vital-from-build to account for libraries removed in
      repack
    * Expand dversionmangle in d/watch to remove numbers after "+dfsg"
  * Introduce ghdl-common package to avoid a circular dependency
    (Closes: #916485)
    * Remove Multi-Arch: same on the ghdl package for now
  * Install all docs in ghdl-common and make other doc directories symlinks
  * Update copyright years in d/copyright
  * Add copyright and licensing for the testsuite to d/copyright
  * Disable building libghdl for now
  * Remove spelling-error lintian overrides for "Synopsys"
  * Point d/watch at releases/latest on GitHub to pick up the correct release
  * Increase debhelper compat level to 13
  * Bump Standards-Version to 4.5.1

 -- Andreas Bombe <aeb@debian.org>  Fri, 29 Jan 2021 15:46:02 +0100

ghdl (0.37+dfsg-3) unstable; urgency=medium

  * Team upload.
  * Update watchfile to ignore date tags
  * Switch to GCC/GNAT 10. Thanks to Ludovic Brenta (Closes: #970660)
  * Allow builds with LLVM 11 (Closes: #976233)

 -- Jonathan McDowell <noodles@earth.li>  Sat, 05 Dec 2020 13:36:28 +0000

ghdl (0.37+dfsg-2) unstable; urgency=medium

  * Team upload.

  [ Frédéric Bonnard ]
  * Really fix FTBFS on ppc64* (Closes: #907170)

 -- Jonathan McDowell <noodles@earth.li>  Sun, 17 May 2020 10:07:15 +0100

ghdl (0.37+dfsg-1) unstable; urgency=medium

  * Team upload.
  * New upstream release
    * Refresh modify-install-paths
    * Remove patch allowing LLVM 8 (upstreamed)
    * Fixes build with LLVM 9 (Closes: #952110)
    * Update wrapper for newer releases. Thanks to Pavel Pisa (Closes: #930890)
  * Switch to GCC 9 (Closes: #944181)

 -- Jonathan McDowell <noodles@earth.li>  Mon, 06 Apr 2020 11:57:15 +0100

ghdl (0.35+git20181129+dfsg-4) unstable; urgency=medium

  * Team upload.
  * Allow building with LLVM 8+. Patch thanks to Logan Rosen (Closes: #923190)

 -- Jonathan McDowell <noodles@earth.li>  Sun, 29 Sep 2019 18:05:58 +0100

ghdl (0.35+git20181129+dfsg-3) unstable; urgency=medium

  * Team upload.

  [ Matthias Klose ]
  * Don't hardcode the gcc tarball name (Closes: #916412)

  [ Jonathan McDowell ]
  * Set Rules-Requires-Root to no
  * Remove debian/source/options - xz is now the default

  [ Frédéric Bonnard ]
  * Fix FTBFS on ppc64* (Closes: #907170)

 -- Jonathan McDowell <noodles@earth.li>  Fri, 22 Feb 2019 15:36:23 +0000

ghdl (0.35+git20181129+dfsg-2) unstable; urgency=medium

  * Team upload.
  * Update debian/copyright to cover additional source files

 -- Jonathan McDowell <noodles@earth.li>  Thu, 13 Dec 2018 22:39:47 +0000

ghdl (0.35+git20181129+dfsg-1) unstable; urgency=medium

  * New upstream release
    * Refresh modify-install-paths
    * Allow builds with LLVM 7+ (Closes: #912564)
  * Switch to GCC 8 (Closes: #915734)
  * Add dependency on zlib1g-dev for gcc and LLVM backends (Closes: #913945)

 -- Jonathan McDowell <noodles@earth.li>  Tue, 11 Dec 2018 20:16:23 +0000

ghdl (0.35+git20180503+dfsg-2) unstable; urgency=medium

  * Fix disabling mcode backend building on non-x86

 -- Andreas Bombe <aeb@debian.org>  Mon, 06 Aug 2018 15:42:10 +0800

ghdl (0.35+git20180503+dfsg-1) unstable; urgency=medium

  * Reintroduce ghdl package with upstream git commit v0.35-168-g252a9169
    (Closes: #880942)
  * Redo most of the packaging to adapt to significant upstream changes
  * ghdl now offers different compiler backends, offer them all in
    co-installable packages ghdl-gcc, ghdl-mcode and ghdl-llvm
  * Add build profiles to skip building any of the backends
  * New /usr/bin/ghdl wrapper script that executes an installed ghdl backend
    variant depending on an environment variable or automatic selection
  * Rewrite debian/copyright, change to machine-readable format
  * Change debian/watch to current upstream on GitHub
  * Change source format to 3.0 (quilt)
  * Change Vcs-* fields in debian/control to point to new location

 -- Andreas Bombe <aeb@debian.org>  Sat, 04 Aug 2018 03:24:44 +0800

ghdl (0.29+gcc4.3.4+dfsg-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Fix FTBFS on multiarch systems. (Closes: #635923)
  * Don't build the package twice.
  * Whole changes have been merged from Ubuntu.

 -- Artur Rona <ari-tczew@tlen.pl>  Sat, 05 May 2012 23:04:42 +0200

ghdl (0.29+gcc4.3.4+dfsg-1) unstable; urgency=low

  * New upstream release (closes: #548004)
  * Updated to policy 3.8.3
  * Now using gcc-4.3.4
  * Now building with gnat-4.4

 -- Wesley J. Landaker <wjl@icecavern.net>  Sun, 24 Jan 2010 16:09:58 -0700

ghdl (0.27+svn110+gcc4.3.3+dfsg-1) unstable; urgency=low

  * Now using gcc-4.3.3
  * Updated copyright information
  * Added Vcs-Git information
  * Updated to policy 3.8.1 

 -- Wesley J. Landaker <wjl@icecavern.net>  Thu, 02 Apr 2009 21:23:07 -0600

ghdl (0.27+svn110+gcc4.3.2~dfsg-3) unstable; urgency=low

  * Ensure that gnatbind uses the correct CC, not cc
  * Added Vcs-Bzr information

 -- Wesley J. Landaker <wjl@icecavern.net>  Wed, 05 Nov 2008 16:13:37 -0700

ghdl (0.27+svn110+gcc4.3.2~dfsg-2) unstable; urgency=low

  * Moving to unstable.
  * Updated to policy 3.8.0

 -- Wesley J. Landaker <wjl@icecavern.net>  Sun, 02 Nov 2008 19:45:31 -0700

ghdl (0.27+svn110+gcc4.3.2~dfsg-1) experimental; urgency=low

  * New upstream release (0.27)
  * Upstream patched to svn110
  * Now using gcc-4.3.2
  * Added newly necessary libgmp3-dev and libmpfr-dev to build-depends.

 -- Wesley J. Landaker <wjl@icecavern.net>  Sun, 02 Nov 2008 16:41:28 -0700

ghdl (0.26+svn98+gcc4.1.2~dfsg-1) unstable; urgency=low

  * Upstream patched to svn98
  * Fix lintian warnings about harmless empty directories.
  * Fix dependency; seems like gnat-4.3 is needed after all.

 -- Wesley J. Landaker <wjl@icecavern.net>  Mon, 02 Jun 2008 17:01:11 -0600

ghdl (0.26+svn94+gcc4.1.2~dfsg-2) unstable; urgency=low

  * Force *FLAGS environment passed to make to override the messed up
    dpkg-buildpackage environment that causes compile failures.

 -- Wesley J. Landaker <wjl@icecavern.net>  Sat, 17 May 2008 12:51:39 -0600

ghdl (0.26+svn94+gcc4.1.2~dfsg-1) unstable; urgency=low

  * Upstream patched to svn94
  * Reworked package build quite a bit; special thanks to Fedora maintainer
    Thomas Sailer <t.sailer@alumni.ethz.ch> for some ideas
  * Finally building with gnat-4.3 (closes: #478032)
  * Revamped package structure and build system

 -- Wesley J. Landaker <wjl@icecavern.net>  Sat, 17 May 2008 11:14:19 -0600

ghdl (0.26+gcc4.1.2~dfsg-4) unstable; urgency=low

  * Fix hosed package generation.

 -- Wesley J. Landaker <wjl@icecavern.net>  Thu, 15 May 2008 18:36:42 -0600

ghdl (0.26+gcc4.1.2~dfsg-3) unstable; urgency=low

  * Reverting back to gnat-4.1, because gnat-4.2 is being removed and
    gnat-4.3 doesn't yet work for building (closes: #478032)
  * Dropped ivi suggestion, as it is obsolete and not in Debian anymore 

 -- Wesley J. Landaker <wjl@icecavern.net>  Thu, 15 May 2008 17:39:18 -0600

ghdl (0.26+gcc4.1.2~dfsg-2) unstable; urgency=low

  * Now building with gnat-4.2.

 -- Wesley J. Landaker <wjl@icecavern.net>  Sun, 12 Aug 2007 10:02:56 -0600

ghdl (0.26+gcc4.1.2~dfsg-1) unstable; urgency=low

  * New upstream release (closes: #413088)
  * Using DFSGized gcc4.1.2 tarball (closes: #392949)
  * Specifically note copyright status of standard headers (closes: #403621)

 -- Wesley J. Landaker <wjl@icecavern.net>  Thu, 12 Apr 2007 18:43:56 -0600

ghdl (0.25+gcc4.1.1-1) unstable; urgency=low

  * New upstream release
  * Now using dpatch

 -- Wesley J. Landaker <wjl@icecavern.net>  Mon, 14 Aug 2006 09:35:06 -0600

ghdl (0.24+gcc4.1.1-1) unstable; urgency=low

  * New upstream release
  * Removed ortho-lang amd64 patch (integrated upstream)
  * Upstream now includes its own manpage; use that instead

 -- Wesley J. Landaker <wjl@icecavern.net>  Sat,  8 Jul 2006 08:45:09 -0600

ghdl (0.23+gcc4.1.1-1) unstable; urgency=medium

  * New upstream release, plus patch from author (closes: #360382)
  * Using gcc-core-4.1.1
  * Building with gnat-4.1
  * Moved to Standards-Version 3.7.2; no changes required

 -- Wesley J. Landaker <wjl@icecavern.net>  Tue, 13 Jun 2006 17:12:56 -0600

ghdl (0.22-1) unstable; urgency=low

  * New upstream release
  * Fixed typo in manpage (closes: #350162)
    * Thanks to Nicolas François <nicolas.francois@centraliens.net>
  * Added texinfo and zlib1g-dev to build-depends.

 -- Wesley J. Landaker <wjl@icecavern.net>  Sat,  1 Apr 2006 09:20:06 -0700

ghdl (0.21-1) unstable; urgency=low

  * The "it-finally-works-on-my-amd64-computer-woohoo!" release.
  * New upstream release (closes: #338427)
  * Disable multilib and mudflap (closes: #276399)
  * Added dependency on zlib1g-dev (closes: #341330)
  * Fixed up debian/rules to use stamp files more cleanly
  * Removed some now-unnecessary patches, applied upstream

 -- Wesley J. Landaker <wjl@icecavern.net>  Sat, 31 Dec 2005 09:39:10 -0700

ghdl (0.20-1) unstable; urgency=low

  * New upstream release
  * Using gcc-core-4.0.2
  * Removed runtime GNAT dependency (closes: #315569, #305045)
  * Added debian/watch
  * Removed some now unnecessary lintian-overrides
  * Updated FSF address to appease the lintian gods

 -- Wesley J. Landaker <wjl@icecavern.net>  Wed,  9 Nov 2005 09:46:36 -0700

ghdl (0.18-2) unstable; urgency=medium

  * Add dependancy on gnat-3.4 (closes: #301875)
  * Urgency medium as this prevents elaboration from working.

 -- Wesley J. Landaker <wjl@icecavern.net>  Mon, 28 Mar 2005 17:42:00 -0700

ghdl (0.18-1) unstable; urgency=low

  * New upstream release
  * Using gcc-core-3.4-20050311

 -- Wesley J. Landaker <wjl@icecavern.net>  Thu, 17 Mar 2005 10:50:18 -0700

ghdl (0.17-3) unstable; urgency=low

  * Added explicit license statement in manpage.

 -- Wesley J. Landaker <wjl@icecavern.net>  Thu, 17 Mar 2005 10:08:18 -0700

ghdl (0.17-2) unstable; urgency=low

  * Added dependency on flex and bison

 -- Wesley J. Landaker <wjl@icecavern.net>  Sat,  5 Mar 2005 08:09:44 -0700

ghdl (0.17-1) unstable; urgency=low

  * New upstream release
  * Using gcc-core-3.4-20050128

 -- Wesley J. Landaker <wjl@icecavern.net>  Fri,  4 Mar 2005 09:14:01 -0700

ghdl (0.16-1) unstable; urgency=low

  * New upstream release
  * No longer need fix_cf_files.sh (fixed upstream)
  * Standards version bumped to 3.6.1.1
  * Added lintian override to let me say "VHDL" in description synopsys

 -- Wesley J. Landaker <wjl@icecavern.net>  Fri, 21 Jan 2005 11:45:01 -0700

ghdl (0.14-2) unstable; urgency=low

  * Moving build to gnat-3.4

 -- Wesley J. Landaker <wjl@icecavern.net>  Thu,  9 Sep 2004 10:43:54 -0600

ghdl (0.14-1) unstable; urgency=low

  * New upstream release
  * Removed build dependency on sed and findutils (essential packages)
  * Removed DESTDIR and MAX_BITS_PER_WORD patches (applied upstream)

 -- Wesley J. Landaker <wjl@icecavern.net>  Sun, 29 Aug 2004 11:52:50 -0600

ghdl (0.13-4) unstable; urgency=low

  * Add overrides for harmless lintian warnings
  * Fixed bad paths getting into .cf files (closes: #268149)

 -- Wesley J. Landaker <wjl@icecavern.net>  Thu, 26 Aug 2004 09:34:46 -0600

ghdl (0.13-3) unstable; urgency=low

  * Added patch for MAX_BITS_PER_WORD support on ia64 and alpha.
    (Closes: #266792)

 -- Wesley J. Landaker <wjl@icecavern.net>  Thu, 19 Aug 2004 07:27:14 -0600

ghdl (0.13-2) unstable; urgency=low

  * Only known to work on i386, but changed architecture to 'any' so the
    buildd's will try.

 -- Wesley J. Landaker <wjl@icecavern.net>  Mon, 16 Aug 2004 20:05:02 -0600

ghdl (0.13-1) unstable; urgency=low

  * Initial Release (closes: #170583)
  * Using GCC 3.4.1 sources
  * Patched GHDL to support DESTDIR

 -- Wesley J. Landaker <wjl@icecavern.net>  Sat, 31 Jul 2004 20:04:56 -0600

